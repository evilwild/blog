import styles from "../styles/Tag.module.css";
import { BASE } from '../helpers/Network';
import Link from 'next/link';
import React from "react";
import PropTypes from "prop-types";

const Tag = (props) => {
  const {name, id} = props;

  const link = `${BASE}/theme/${id}`;

  return (
    <Link href={link}>
      <div className={styles.Tag}>{name}</div>
    </Link>
  )
};

Tag.propTypes = {
  name: PropTypes.string,
  id: PropTypes.number,
};

export default Tag;
