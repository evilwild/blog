import React, { useState, useEffect } from "react";
import styles from "../styles/FetchedItem.module.css";
import PropTypes from "prop-types";

const _ = require("lodash");

const FetchedItem = (props) => {
  let [error, setError] = useState(null);
  let [isFetching, setIsFetching] = useState(true);
  let [data, setData] = useState({});


  useEffect(() => {
    const axios = require("axios");
    axios
      .get(props.link)
      .then((res) => {
        setIsFetching(false);
        setData(res.data);
      })
      .catch((err) => {
        setIsFetching(false);
        setError(err);
      });
  }, []);


  return (
    <React.Fragment>
      {error ? (
        <div className={styles.Error}>Ошибка при загрузке: {error.message}</div>
      ) : null}
      {!isFetching ? (
        _.isEmpty(data) ? (
          <div>Записей нет</div>
        ) : (
          React.cloneElement(props.children, { payload: data , ...props.children.props})
        )
      ) : (
        <div className={styles.Loading}>Загрузка...</div>
      )}
    </React.Fragment>
  );
};

FetchedItem.propTypes = {};

export default FetchedItem;
