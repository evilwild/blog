import React from "react";
import PropTypes from "prop-types";
import styles from "../styles/Layout.module.css";
import AppHeader from "./AppHeader";
import AppFooter from "./AppFooter";

export default class Layout extends React.Component {
  render() {
    const { children } = this.props;

    return (
      <div className={styles.Layout}>
        <AppHeader />
        <div className={styles.Content}>{children}</div>
        <AppFooter />
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.object,
};
