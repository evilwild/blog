import React from "react";
import styles from "../styles/AppButton.module.css";
import Link from 'next/link';
import PropTypes from "prop-types";

const AppButton = (props) => {
  const { className, onClick, link } = props;

  return (
    <Link href={link ? link : ''}>
      <button onClick={onClick} className={`${styles.AppButton} ${className}`}>
        {props.children}
      </button>
    </Link>
  );
};

AppButton.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string || PropTypes.object,
  onClick: PropTypes.func,
};

export default AppButton;
