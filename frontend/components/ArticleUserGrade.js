import React from "react";
import styles from "../styles/ArticleUserGrade.module.css";
import PropTypes from "prop-types";

export default class ArticleUserGrade extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isShown: false,
    };
  }

  setIsShown(x) {
    this.setState({
      isShown: x,
    });
  }

  calculateSummary(good, bad) {
    if (good > 0 && bad > 0) {
      return Math.round((good / (good + bad)) * 100, 10);
    }
    return 0;
  }

  render() {
    const { good, bad } = this.props;

    const summary = this.calculateSummary(good, bad);

    return (
      <div className={styles.UserGradeSummaryContainer}>
        <div
          onMouseEnter={() => this.setIsShown(true)}
          onMouseLeave={() => this.setIsShown(false)}
          className={`${styles.UserGradeSummary} ${
            good => bad
              ? styles["UserGradeSummary--good"]
              : styles["UserGradeSummary--bad"]
          }`}
        >
          {summary === 0 ? <div>Нет оценок</div> : summary + "%"}
        </div>
        {this.state.isShown && (
          <div className={styles.UserGradeSummaryHover}>
            Good - {good}
            <br />
            Bad - {bad}
          </div>
        )}
      </div>
    );
  }
}

ArticleUserGrade.propTypes = {
  good: PropTypes.number,
  bad: PropTypes.number,
};
