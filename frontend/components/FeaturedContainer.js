import styles from "../styles/FeaturedContainer.module.css";
import ArticlePreview from "./ArticlePreview";
import React from "react";
import PropTypes from "prop-types";

export default class FeaturedContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentArticle: 0,
    };
  }

  handleSliderClick(i) {
    this.setState(() => ({
      currentArticle: i,
    }));
  }

  render() {
    const { articles } = this.props;

    if (articles && articles.length < 1) {
      return (
        <div className={styles.FeaturedContainer}>
          <div className={styles.NoArticles}>
            В настоящее время статей нет ):
          </div>
        </div>
      );
    }

    const sliderBalls = [];

    for (var i = 0, len = articles.length; i < len; i++) {
      sliderBalls.push(
        <div
          key={i}
          className={`${styles.SliderBall} ${
            this.state.currentArticle === i ? styles["SliderBall--active"] : ""
          }`}
          onClick={this.handleSliderClick.bind(this, i)}
        />
      );
    }

    const featuredArticles = articles.map((article, index) => {
      return (
        <ArticlePreview
          key={index}
          id={article.id}
          isFeatured
        />
      );
    });

    return (
      <div className={styles.FeaturedContainer}>
        <div className={styles.CurrentArticle}>
          {featuredArticles[this.state.currentArticle]}
        </div>
        <div className={styles.Slider}>{sliderBalls}</div>
      </div>
    );
  }
}

FeaturedContainer.propTypes = {
  articles: PropTypes.array,
};
