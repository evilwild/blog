import styles from "../styles/AppFooter.module.css";
import React from "react";

export default class AppFooter extends React.Component {
  constructor() {
    super();
  }

  render() {
    return <footer className={styles.Footer}>Footer</footer>;
  }
}
