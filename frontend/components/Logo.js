import styles from "../styles/Logo.module.css";
import Link from "next/link";
import React from "react";

export default class Logo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Link href="/">
        <div className={styles.Wrapper}>
          <img className={styles.Logo} src="/logo.svg" height="36" width="36" />
        </div>
      </Link>
    );
  }
}
