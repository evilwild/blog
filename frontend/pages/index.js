import React from "react";
import styles from "../styles/Index.module.css";
import { API } from "../helpers/Network";
import Head from "next/head";
import axios from '../helpers/axios';
import SectionDivider from "../components/SectionDivider";
import FeaturedContainer from "../components/FeaturedContainer";
import ArticlePreview from "../components/ArticlePreview";
import AppButton from "../components/AppButton";

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      articles: [],
    };
  }

  onLoadClick() {
    console.log("Load more...");
  }

  componentDidMount() {
    const link = API + "/articles/0";
    axios
      .get(link)
      .then((res) => {
        this.setState({ isLoaded: true, articles: res.data });
      })
      .catch((err) => {
        this.setState({ isLoaded: true, err });
      });
  }

  render() {
    const { error, isLoaded, articles } = this.state;

    if (error) {
      return <div>Error {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      let articlesPreview;

      if (articles && articles.length === 0) {
        articlesPreview = <div>В настоящий момент статей нет ):</div>;
      } else {
        articlesPreview = articles.map((article, index) => {
          return <ArticlePreview key={index} id={article.id} />;
        });
      }

      return (
        <div>
          <Head>
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1, maximum-scale=1"
            />
            <title>Blog Homepage</title>
            />
          </Head>
          <SectionDivider name="Интересное" />
          <section>
            <FeaturedContainer articles={articles} />
          </section>
          <SectionDivider name="Публикации" />
          <section>{articlesPreview}</section>
          {articles &&
            articles.length !==
              (
                <div className={styles.ButtonContainer}>
                  <AppButton
                    onClick={this.onLoadClick.bind(this)}
                    className={styles.Button}
                  >
                    Загрузить ещё
                  </AppButton>
                </div>
              )}
        </div>
      );
    }
  }
}
