import React, { useState } from 'react'
import { API } from '../../../helpers/Network';
import axios from '../../../helpers/axios';
import AppButton from '../../../components/AppButton';

const NewTheme = (props) => {
  const [ title, setTitle ] = useState('');
  const [ msg, setMsg ] = useState(null);
  const [ error, setError ] = useState(null);
  
  const onPublishClick = (e) => {
    e.preventDefault();

    axios.post(API + '/theme', { title })
      .then((res) => {
        setMsg(`Successfully created cycle with ${res.data.id}`);
        setError(null);
      })
      .catch((err) => {
        setError(err);
        setMsg(null);
      });
  }

  const onTitleChange = (e) => {
    setTitle(e.target.value);
  }

  return <section>
    <h1>Новая тема</h1>

    <form>
      <label>
        Название
        <input onChange={onTitleChange} type="text" />
      </label>
      <AppButton onClick={onPublishClick}>Опубликовать</AppButton>
    </form>

    { error && (
      <div className="Error">{ error.message }</div>
    ) }
    {  }
    { msg && (
      <div className="Response">{ msg }</div>
    ) }
    
  </section>
}

export default NewTheme;
