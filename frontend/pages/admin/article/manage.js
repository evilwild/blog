import React, { useState } from "react";
import PropTypes from 'prop-types';
import axios, { getData } from "../../../helpers/axios";
import { useRouter } from "next/router";
import AppButton from "../../../components/AppButton";
import { API } from "../../../helpers/Network";

const ManageArticlePage = (props) => {
  const { id, cycles, themes } = props;
  const router = useRouter();
  const [article, setArticle] = useState(props.article);
  const [formTitle, setFormTitle] = useState('');
  const [formText, setFormText] = useState('');
  const [formCycle, setFormCycle] = useState({});
  const [formThemes, setFormThemes] = useState([]);

  const isEdit = (id) ? true: false;

  const onSubmitBtnClick = (e, id = null) => {
    e.preventDefault();

    const link = isEdit ? `${API}/article/${article.id}` : `${API}/article`;

    const data = (article) ? article : { title: formTitle, text: formText, cycle: formCycle, themes: formThemes };

    axios
      .post(link, data)
      .then((res) => router.push(`/article/${res.data.id}`))
      .catch((err) => console.error(err));
  };

  const handleTitleChange = (e) => {
    if (isEdit) {
      setArticle({ ...article, title: e.target.value });
      return;
    }
    setFormTitle(e.target.value);
  };

  const handleTextChange = (e) => {
    if (isEdit) {
      setArticle({ ...article, text: e.target.value })
      return;
    }
    setFormText(e.target.value);
  }

  const handleCycleChange = (e) => {
    const selectedOption = [...e.target.options].filter((x) => x.selected)[0];
    const dataValue = selectedOption.dataset.value;
    const data = dataValue ? dataValue : null;
    if (isEdit) {
      setArticle({ ...article, cycle: JSON.parse(data) });
      return;
    }
    setFormCycle(JSON.parse(data));
  }

  const handleThemeChange = (e) => {
    const selectedOptions = [...e.target.options].filter((x) => x.selected);
    const _themes = selectedOptions.map((option) => {
      return option.dataset.value ? JSON.parse(option.dataset.value) : null;
    });
    if (isEdit) {
      setArticle({ ...article, themes: _themes });
      return;
    }
    setFormThemes(_themes);
  }

  const defaultTitleValue = () => {
    if (article && article.title) return article.title;
    return "";
  };

  const defaultTextValue = () => {
    if (article && article.text) return article.text;
    return "";
  }

  const defaultCycleValue = () => {
    if (article && article.cycle && article.cycle.id) return article.cycle.id;
    return "";
  }

  const defaultThemeValue = () => {
    if (article && article.themes) {
      return article.themes.map((theme) => (theme ? theme.id : null));
    }
    return [];
  }

  const headerMessage = id
    ? `Edit article with ${id} id`
    : `Create new article`;
  const submitButtonMessage = id ? "Edit article" : "Create article";

  return (
    <section>
      <h1>{headerMessage}</h1>
      <div>
        <form>

          <label>
            Title:
            <input
              name="title"
              onChange={handleTitleChange}
              defaultValue={defaultTitleValue()}
              type="text"
            />
          </label>

          <label>
            Text:
            <input 
              name="text"
              onChange={handleTextChange}
              defaultValue={defaultTextValue()}
            />
          </label>

          <label>
            Cycle:
            <select 
              defaultValue={defaultCycleValue()}
              onChange={handleCycleChange}
              name="cycle"
            >
              <option value="">No cycle</option>
                {
                  cycles.map((cycle) => {
                  return (
                    <option
                      key={cycle.id}
                      value={cycle.id}
                      data-value={JSON.stringify(cycle)}
                    >
                      {cycle.title}
                    </option>
                  );
                })
              }
            </select>
          </label>

          <label>
            Themes:
            <select
              defaultValue={defaultThemeValue()} 
              onChange={handleThemeChange}
              multiple
              name="themes"
            >
              <option value="">No theme</option>
              {themes.map((theme) => {
                return (
                  <option
                    key={theme.id}
                    value={theme.id}
                    data-value={JSON.stringify(theme)}
                  >
                    {theme.title}
                  </option>
                );
              })}
            </select>
          </label>

          <AppButton onClick={onSubmitBtnClick}>{submitButtonMessage}</AppButton>
        </form>
      </div>
    </section>
  );
};

export async function getServerSideProps(context) {
  let { id } = context.query;
  id = id ? id : null;

  let article = null;
  if (id) {
    article = await getData(`${API}/article/${id}`);
  }

  const themes = await getData(`${API}/themes`);

  const cycles = await getData(`${API}/cycles`);

  return { props: { id, themes, cycles, article } };
}

ManageArticlePage.propTypes = {
  id: PropTypes.string || null,
  themes: PropTypes.array,
  cycle: PropTypes.object,
}

export default ManageArticlePage;
