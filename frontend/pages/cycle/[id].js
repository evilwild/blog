import PropTypes from "prop-types";
import React from "react";
import ArticlePreview from "../../components/ArticlePreview";
import SectionDivider from "../../components/SectionDivider";
import { API } from "../../helpers/Network";
import styles from "../../styles/CyclePage.module.css";
import FetchedItem from "../../components/FetchedItem";

const Cycle = (props) => {
  const { payload } = props;

  return (
    <section>
      <h1 className={styles.Title}>{payload.title}</h1>
      <p className={styles.Description}>{payload.description}</p>
      <SectionDivider name="Публикации" />
      <div className={styles.Articles}>
        {payload.articles.length === 0 ? (
          <div className={styles.NoArticles}>
            С циклом не связана ни одна статья ):
          </div>
        ) : (
          payload.articles.map((article) => {
            return <ArticlePreview key={article.id} id={article.id} />;
          })
        )}
      </div>
    </section>
  );
};

const CyclePage = (props) => {
  const { id } = props;
  const link = API + "/cycle/" + props.id;

  return (
    <FetchedItem link={link}>
      <Cycle />
    </FetchedItem>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.params;

  return { props: { id } };
}

CyclePage.propTypes = {
  cycle: PropTypes.object,
};

export default CyclePage;
