import React from "react";
import { API } from "../helpers/Network";
import Link from "next/link";
import styles from "../styles/CyclesPage.module.css";
import PropTypes from "prop-types";
import FetchedItem from "../components/FetchedItem";

const CycleContainer = (props) => {
  if (props.payload.length) {
    return props.payload.map((cycle, index) => {
      const link = `cycle/${cycle.id}`;
      return (
        <Link href={link} key={index}>
          <div className={styles.Cycle}>
            <h2 className={styles.Title}>{cycle.title}</h2>
            <p className={styles.Description}>{cycle.description}</p>
          </div>
        </Link>
      );
    });
  }
  return <div>Not loaded</div>;
};

const CyclesPage = (props) => {
  const linkApi = API + "/cycles";
  const DOM = (
    <FetchedItem link={linkApi}>
      <CycleContainer></CycleContainer>
    </FetchedItem>
  );

  return (
    <section>
      <h1>Циклы</h1>
      <div className={styles.Container}>{DOM}</div>
    </section>
  );
};

export default CyclesPage;
