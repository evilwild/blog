import React from "react";
import { API } from "../helpers/Network";
import styles from "../styles/AdminPage.module.scss";
import axios from "../helpers/axios";
import AdminEntityTable, { ColumnName } from "../components/AdminEntityTable";

const AdminPage = (props) => {
  const { articlesRes, cyclesRes, themesRes } = props;
  
  const ratingFilter = (v) => {
    const { good, bad } = v;
    const count = good + bad;
    return count === 0 ? 0 : good / count;
  };

  const timestampFilter = (v) => {
    return new Date(v).toLocaleString("ru-RU");
  };

  const articleTableColumns = [
    ColumnName("id", "ID"),
    ColumnName("title", "Title"),
    ColumnName("text", "Text", (v) => v.slice(0, 30)),
    ColumnName("views", "Views", (v) => v.count),
    ColumnName("rating", "Rating", ratingFilter),
    ColumnName("comments", "Comments", (v) => v.length),
    ColumnName("createdAt", "Created at", timestampFilter),
    ColumnName("modifiedAt", "Modified at", timestampFilter),
  ];

  const cycleTableColumns = [
    ColumnName("title", "Title"),
    ColumnName("description", "Description"),
    ColumnName("articles", "Articles count", (v) => v.length),
  ];

  const themeTableColumns = [
    ColumnName("title", "Title"),
    ColumnName("articles", "Articles count", (v) => v.length),
  ];

  return (
    <section className={styles.Section}>
      <h1>Менеджер сайта</h1>
      <AdminEntityTable
        columns={articleTableColumns}
        entities={articlesRes}
        header="Публикации"
        emptyMessage="Публикации отсутствуют"
        newLink="/admin/article/manage"
        newButtonText="Создать публикацию"
        isPageble={true}
        loadNewPageLink={(page) => `${API}/articles/${page}`}
        editLink={(id) => `/admin/article/manage?id=${id}`}
        deleteLink={(id) => `${API}/article/${id}`}
      />
      <AdminEntityTable
        columns={cycleTableColumns}
        entities={cyclesRes}
        header="Циклы"
        emptyMessage="Циклы отсутствуют"
        newLink="/admin/cycle/new"
        newButtonText="Создать цикл"
        editLink={(id) => `/cycle/${id}/edit`}
        deleteLink={(id) => `${API}/cycle/${id}`}
      />
      <AdminEntityTable
        columns={themeTableColumns}
        entities={themesRes}
        header="Темы"
        emptyMessage="Темы отсутствуют"
        newLink="/admin/theme/new"
        newButtonText="Создать тему"
        editLink={(id) => `/theme/${id}/edit`}
        deleteLink={(id) => `${API}/theme/${id}`}
      />
    </section>
  );
};

export async function getServerSideProps() {
  const fetchData = (url) => {
    return axios
      .get(url)
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        return err;
      });
  };

  const articlesRes = await fetchData(API + "/articles/0");
  const cyclesRes = await fetchData(API + "/cycles");
  const themesRes = await fetchData(API + "/themes");

  return { props: { articlesRes, cyclesRes, themesRes } };
}

export default AdminPage;
