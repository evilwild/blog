import React from "react";
import { BASE, API } from "../helpers/Network";
import Link from "next/link";
import styles from "../styles/ThemesPage.module.css";
import PropTypes from "prop-types";

const ThemesPage = (props) => {
  const { themes } = props;

  const themesDOM = themes.map((theme, index) => {
    const link = `${BASE}/theme/${theme.id}`;
    return (
      <Link href={link} key={index}>
        <div className={styles.Theme}>{theme.title}</div>
      </Link>
    );
  });

  return (
    <section>
      <h1>Темы</h1>
      {themesDOM.length === 0 && (
        <div className="Error">В настоящий момент тем нет ):</div>
      )}
      <div className={styles.Themes}>{themesDOM}</div>
    </section>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(API + "/themes");
  const themes = await res.json();

  return { props: { themes } };
}

ThemesPage.propTypes = {
  themes: PropTypes.array,
};

export default ThemesPage;
