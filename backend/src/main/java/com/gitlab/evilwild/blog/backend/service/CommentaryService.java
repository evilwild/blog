package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.CommentaryEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.CommentaryRepository;

@Service
public class CommentaryService extends AbstractService<CommentaryEntity, CommentaryRepository, Long>{

	@Autowired
	public CommentaryService(final CommentaryRepository repository) {
		super(repository);
	}

	@Override
	public CommentaryEntity update(CommentaryEntity newCommentary, Long id) {
		CommentaryEntity updated = repository.findById(id)
				.map(commentary -> {
					commentary
						.setAuthor(newCommentary.getAuthor())
						.setText(newCommentary.getText());
					return repository.save(commentary);
				})
				.orElseThrow(() -> new EntityNotFoundException());

		return updated;
	}
}
