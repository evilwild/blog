package com.gitlab.evilwild.blog.backend.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="Theme")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class ThemeEntity extends AbstractEntity {

	private static final long serialVersionUID = -7805021642150353921L;

	@Column(name="theme_title")
	String title;

	@ManyToMany(mappedBy = "themes")
	List<ArticleEntity> articles;

	public ThemeEntity(String title) {
		this.title = title;
	}
}
