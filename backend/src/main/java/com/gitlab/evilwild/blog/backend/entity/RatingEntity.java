package com.gitlab.evilwild.blog.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="rating")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class RatingEntity extends AbstractEntity {

	private static final long serialVersionUID = 4932398474894813361L;

	@Column(name="rating_good", columnDefinition = "integer default 0")
	int good;

	@Column(name="rating_bad", columnDefinition = "integer default 0")
	int bad;
}
