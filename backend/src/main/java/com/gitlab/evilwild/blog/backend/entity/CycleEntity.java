package com.gitlab.evilwild.blog.backend.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="cycle")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class CycleEntity extends AbstractEntity {

	private static final long serialVersionUID = 8076925829085067027L;

	@Column(name="cycle_title")
	String title;

	@Column(name="cycle_desc")
	String description;

	@OneToMany(mappedBy = "cycle")
	List<ArticleEntity> articles;

	public CycleEntity(String title, String description) {
		this.title = title;
		this.description = description;
	}
}
