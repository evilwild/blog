package com.gitlab.evilwild.blog.backend.service;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.AbstractEntity;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class AbstractService<T extends AbstractEntity, R extends JpaRepository<T, K>, K extends Serializable> {
	protected R repository;

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public T getById(final K id) throws EntityNotFoundException {
		final T founded = this.repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		return founded;
	}

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public List<T> getAll() throws EntityNotFoundException {
		final List<T> allEntities = this.repository.findAll();
		return allEntities;
	}

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public List<T> getPageContent(PageRequest pageRequest) throws EntityNotFoundException {
		final List<T> allEntities = this.repository.findAll(pageRequest).getContent();
		return allEntities;
	}

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public T create(T entity) throws EntityNotFoundException {
		return repository.save(entity);
	}

	@Transactional(rollbackOn = EntityNotFoundException.class)
	abstract public T update(T entity, K id) throws EntityNotFoundException;

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public T delete(final K id) throws EntityNotFoundException {
		final T deleted = this.repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		this.repository.delete(deleted);;
		return deleted;
	}
}
