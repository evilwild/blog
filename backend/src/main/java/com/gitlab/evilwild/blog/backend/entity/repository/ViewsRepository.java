package com.gitlab.evilwild.blog.backend.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.ViewsEntity;

public interface ViewsRepository extends JpaRepository<ViewsEntity, Long> {

}
