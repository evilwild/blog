package com.gitlab.evilwild.blog.backend.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.ThemeEntity;

public interface ThemeRepository extends JpaRepository<ThemeEntity, Long> {

}
