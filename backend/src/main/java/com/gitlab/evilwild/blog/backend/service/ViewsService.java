package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.ViewsEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.ViewsRepository;

@Service
public class ViewsService extends AbstractService<ViewsEntity, ViewsRepository, Long>{

	public ViewsService(final ViewsRepository repository) {
		super(repository);
	}

	@Override
	public ViewsEntity update(ViewsEntity updatedViews, Long id) throws EntityNotFoundException {
		return repository.findById(id)
				.map(views -> {
					views
					.setCount(updatedViews.getCount());
					return repository.save(views);
				})
				.orElseThrow(() -> new EntityNotFoundException());
	}

}
