package com.gitlab.evilwild.blog.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.evilwild.blog.backend.entity.ArticleEntity;
import com.gitlab.evilwild.blog.backend.entity.CommentaryEntity;
import com.gitlab.evilwild.blog.backend.entity.RatingEntity;
import com.gitlab.evilwild.blog.backend.entity.ViewsEntity;
import com.gitlab.evilwild.blog.backend.service.ArticleService;
import com.gitlab.evilwild.blog.backend.service.CommentaryService;
import com.gitlab.evilwild.blog.backend.service.RatingService;
import com.gitlab.evilwild.blog.backend.service.ViewsService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
class ArticleController implements ICrudController<ArticleEntity, Long>{

	@Autowired
	private final ArticleService articleService;

	@Autowired
	private final ViewsService viewsService;

	@Autowired
	private final RatingService ratingService;

	@Autowired
	private final CommentaryService commentaryService;

	@GetMapping("/article/{id}/vote/good")
	void voteGood(@PathVariable Long id) {
		RatingEntity rating = articleService.getById(id).getRating();
		ratingService.update(rating.setGood(rating.getGood() + 1), rating.getId());
	}

	@GetMapping("/article/{id}/vote/bad")
	void voteBad(@PathVariable Long id) {
		RatingEntity rating = articleService.getById(id).getRating();
		ratingService.update(rating.setBad(rating.getBad() + 1), rating.getId());
	}

	@GetMapping("/article/{id}/view")
	void view(@PathVariable Long id) {
		ViewsEntity views = articleService.getById(id).getViews();
		viewsService.update(views.setCount(views.getCount() + 1), views.getId());
	}

	@GetMapping("/articles/{num}")
	List<ArticleEntity> getPage(@PathVariable int num) {
		return articleService.getPageContent(PageRequest.of(num, 5, Sort.by(Sort.Direction.DESC, "id")));
	}

	@GetMapping("/articles/featured")
	List<ArticleEntity> getFeatured() {
		return null;
	}

	@GetMapping("/article/{id}/comments")
	List<CommentaryEntity> getComments(@PathVariable long id) {
		ArticleEntity article = articleService.getById(id);
		return article.getComments();
	}

	@PostMapping("/article/{id}/comment")
	void addCommentary(@RequestBody CommentaryEntity commentary, @PathVariable Long id) {
		CommentaryEntity createdCommentary = commentaryService.create(commentary);
		ArticleEntity article = articleService.getById(id);
		List<CommentaryEntity> comments = article.getComments();
		comments.add(createdCommentary);
		article.setComments(comments);
		articleService.update(article, id);
	}

	@Override
	@DeleteMapping("/article/{id}")
	public void delete(@PathVariable Long id) {
		articleService.delete(id);
	}

	@Override
	@PostMapping("/article")
	public ArticleEntity create(@RequestBody ArticleEntity entity) {
		return articleService.create(entity);
	}

	@Override
	@PostMapping("/article/{id}")
	public ArticleEntity update(@RequestBody ArticleEntity newEntity, @PathVariable Long id) {
		return articleService.update(newEntity, id);
	}

	@Override
	@GetMapping("/article/{id}")
	public ArticleEntity get(@PathVariable Long id) {
		return articleService.getById(id);
	}

	@Override
	@GetMapping("/articles")
	public List<ArticleEntity> getAll() {
		return articleService.getAll();
	}
}
