package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.CycleEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.CycleRepository;

@Service
public class CycleService extends AbstractService<CycleEntity, CycleRepository, Long>{

	public CycleService(final CycleRepository repository) {
		super(repository);
	}

	@Override
	public CycleEntity update(CycleEntity newCycle, Long id) {
		return repository.findById(id)
				.map(cycle -> {
					cycle
						.setTitle(newCycle.getTitle())
						.setDescription(newCycle.getDescription());
					return repository.save(cycle);
				})
				.orElseThrow(() -> new EntityNotFoundException());
	}

}
