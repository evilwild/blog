package com.gitlab.evilwild.blog.backend.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.CommentaryEntity;

public interface CommentaryRepository extends JpaRepository<CommentaryEntity, Long> {

}
