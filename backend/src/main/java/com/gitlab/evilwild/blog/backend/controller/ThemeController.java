package com.gitlab.evilwild.blog.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.evilwild.blog.backend.entity.ThemeEntity;
import com.gitlab.evilwild.blog.backend.service.ThemeService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
class ThemeController implements ICrudController<ThemeEntity, Long> {

	@Autowired
	private final ThemeService themeService;

	@Override
	@PostMapping("/theme")
	public ThemeEntity create(@RequestBody ThemeEntity theme) {
		return themeService.create(theme);
	}

	@Override
	@PutMapping("/theme/{id}")
	public ThemeEntity update(@RequestBody ThemeEntity newTheme, @PathVariable Long id) {
		return themeService.update(newTheme, id);
	}

	@Override
	@DeleteMapping("/theme/{id}")
	public void delete(@PathVariable Long id) {
		themeService.delete(id);
	}

	@Override
	@GetMapping("/theme/{id}")
	public ThemeEntity get(@PathVariable Long id) {
		return themeService.getById(id);
	}

	@Override
	@GetMapping("/themes")
	public List<ThemeEntity> getAll() {
		return themeService.getAll();
	}
}
