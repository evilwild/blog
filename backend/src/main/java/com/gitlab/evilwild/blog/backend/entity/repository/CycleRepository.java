package com.gitlab.evilwild.blog.backend.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.CycleEntity;

public interface CycleRepository extends JpaRepository<CycleEntity, Long> {

}
