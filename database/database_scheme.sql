create domain names_dom as varchar(65536) check (length(value) >= 1);
create domain texts_dom as text check (length(value) >= 1);
create domain views_dom as int default 0;
create domain time_dom as timestamp;
create domain image_dom as varchar(64) check (length(value) >= 1);/* UUID */

create table if not exists THEMES(
	theme_name names_dom primary key not null
);

create table if not exists CYCLES(
	cycle_name names_dom primary key not null
);

create table if not exists ARTICLES(
	article_id serial unique,
	article_name names_dom not null,
	theme_name names_dom references THEMES(theme_name) on update cascade not null,
	cycle_name names_dom references CYCLES(cycle_name) on update cascade on delete cascade
);

create table if not exists IMAGES(
	image_id serial,
	article_id integer references ARTICLES(article_id) on delete cascade not null
);

create table if not exists TIMES(
	article_id integer references ARTICLES(article_id) on delete cascade not null ,
	publication_time time_dom not null,
	edition_time time_dom,
	primary key(article_id)
);

create table if not exists ARTICLE_VIEWS(
	article_id integer references ARTICLES(article_id) on delete cascade not null ,
	views_count views_dom,
	primary key(article_id)
);

create table if not exists ARTICLE_TEXTS(
	article_id integer references ARTICLES(article_id) on delete cascade not null ,
	article_text texts_dom not null,
	primary key(article_id)
);

create or replace procedure create_article(article_name names_dom, theme_name names_dom, cycle_name names_dom default null)
language plpgsql
as $$
declare
	a_id integer;
begin
	insert into ARTICLES(article_name, theme_name, cycle_name) values (article_name, theme_name , cycle_name) returning article_id into a_id;
	insert into ARTICLE_VIEWS(article_id) values (a_id);
end;
$$;

create or replace procedure edit_article_text(_article_id integer, _article_text texts_dom)
language plpgsql
as $$
begin
		update ARTICLE_TEXTS
		set article_id = _article_id,
		article_text = _article_text
		where article_id  = _article_id;

		update TIMES
		set edition_time = current_timestamp
		where article_id = _article_id;
end;
$$;

create or replace procedure delete_article(_article_id integer)
language plpgsql
as $$
begin
	delete from ARTICLES where article_id = _article_id;
end;
$$;

create or replace procedure create_article_text(article_id integer, article_text texts_dom)
language plpgsql
as $$
begin
	insert into ARTICLE_TEXTS(article_id, article_text) values (article_id, article_text);
	insert into TIMES(article_id, publication_time) values (article_id, current_timestamp);
end;
$$;

create or replace procedure add_image_to_article(article_id integer, image_id image_dom)
language plpgsql
as $$
begin
  insert into IMAGES(image_id, article_id) values(image_id, article_id);
end;
$$;

create or replace procedure remove_image_from_article(image_id image_dom)
language plpgsql
as $$
begin
  delete from IMAGES where image_id = image_id;
end;
$$;

create or replace procedure create_cycle(_cycle_name names_dom)
language plpgsql
as $$
begin
	insert into CYCLES(cycle_name) values (_cycle_name);
end;
$$;

create or replace procedure edit_cycle(_old_cycle_name names_dom, _new_cycle_name names_dom)
language plpgsql
as $$
begin
	update CYCLES
	set cycle_name = _new_cycle_name
	where cycle_name = _old_cycle_name;
end;
$$;

create or replace procedure delete_cycle(_cycle_name names_dom)
language plpgsql
as $$
begin
	delete from CYCLES where cycle_name = _cycle_name;
end;
$$;

create or replace procedure create_theme(_theme_name names_dom)
language plpgsql
as $$
begin
	insert into THEMES(theme_name) values (_theme_name);
end;
$$;

create or replace procedure edit_theme(_old_theme_name names_dom, _new_theme_name names_dom)
language plpgsql
as $$
begin
	update THEMES
	set theme_name = _new_theme_name
	where theme_name = _old_theme_name;
end;
$$;

create or replace procedure delete_theme(_theme_name names_dom)
language plpgsql
as $$
begin
	delete from THEMES where theme_name = _theme_name;
end;
$$;

create or replace procedure increment_views_counter(_article_id integer)
language plpgsql
as $$
begin
	update ARTICLE_VIEWS
	set views_count = views_count + 1
	where article_id = _article_id;
end;
$$;
